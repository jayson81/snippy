 // This program is a client of classes A and Demo

 #include <iostream>
 #include <vector>
 #include <string>
 #include "Demo.h"//include class definition

 using namespace std;


 void myfun(Demo var);

 int main()
 {
  Demo d1, d2;//initialized by default c'tor
  Demo d3 (17, 12.10); //initilized  with overloaded c'tor
  Demo d4 = {21, 13.99}; //different syntax for overloaded c'tor

  //by default we can
  d2  = d3; // default assignment

  //however, by default we cannot print
  //cout<<d1; //error

  myfun(d2);// pass a2 as an argument

    cout<<endl<<"Welcome to class demo..."<<endl;   

    d1.fun();//call the method fun, in object d1    
    //dot operator allows acces to class members



  cout<<"d2.var = "<<d2.getVar()<<endl;
  cout<<"d1.var = "<<d1.getVar()<<endl;
  cout<<"d3.var = "<<d3.getVar()<<endl;
  cout<<"d4.var = "<<d4.getVar()<<endl;

  //  d2.var =5; //error (var is private)

  d2.setVar(5);// correct way

 }//end main

 void myfun(Demo a)
 {
   cout<<"fun with 'a'"<<endl;
   
 }
    
